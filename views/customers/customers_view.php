<h1><?= __("Customer") ?> '<?= $customer['customer_name'] ?>'</h1>
<table class="table table-bordered">

    <tr>
        <th><?= __("Customer") ?> ID</th>
        <td><?= $customer['customer_id'] ?></td>
    </tr>

    <tr>
        <th><?= __("Customer") ?><?= __("name") ?></th>
        <td><?= $customer['customer_name'] ?></td>
    </tr>

</table>

<!-- EDIT BUTTON -->
<?php if ($auth->is_admin): ?>
    <form action="customers/edit/<?= $customer['customer_id'] ?>">
        <div class="pull-right">
            <button class="btn btn-primary">
                <?= __("Edit") ?>
            </button>
        </div>
    </form>
<?php endif; ?>