<h3><?= __("Customers") ?></h3>
<ul class="list-group">
    <?php foreach ($customers as $customer): ?>
        <li class="list-group-item">
            <a href="customers/<?= $customer['customer_id'] ?>/<?= $customer['customer_name'] ?>"><?= $customer['customer_name'] ?></a>
        </li>
    <?php endforeach ?>
</ul>

<?php if ($auth->is_admin): ?>
<h3><?= __("Add new customer") ?></h3>

<form method="post" id="form">
    <form id="form" method="post">
        <table class="table table-bordered">
            <tr>
                <th><?= __("Name") ?></th>
                <td><input type="text" name="data[customer_name]" placeholder=""/></td>
            </tr>
        </table>

        <button class="btn btn-primary" type="submit"><?= __("Add") ?></button>
    </form>
    <?php endif; ?>
