<style>
    td:first-child{
        cursor:pointer;
    }
</style>

<h3><?= __("Invoices") ?></h3>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th><?= __('Customer') ?></th>
        <th><?= __('Sum') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($invoices as $invoice): ?>
        <tr>
            <td><?=$invoice['invoice_id']?></td>
            <td><?=$invoice['customer_name']?></td>
            <td><?=$invoice['sum']?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<script>
    $('td').on('click', function () {
        location.href='invoices/'+$(this).find('tr:first-child').html();
    })
</script>