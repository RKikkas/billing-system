<h3><?= __("Invoice") ?> <?= $invoice['invoice_id'] ?></h3>
<table class="table table-bordered table-striped" style="width:400px">
    <thead>
        <tr>
            <th><?= __('Invoice ID') ?></th>
            <td><?=$invoice['invoice_id']?></td>
        </tr>
        <tr>
            <th><?= __('Customer') ?></th>
            <td><?=$invoice['customer_name']?></td>
        </tr>
        <tr>
            <th><?= __('Sum') ?></th>
            <td><?=$invoice['sum']?></td>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($invoices as $invoice): ?>
        <tr>
            <th><?= __('Customer') ?></th>
            <td><?=$invoice['customer_name']?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th><?= __('Description') ?></th>
        <th><?= __('Price') ?></th>
        <th><?= __('Quantity') ?></th>
        <th><?= __('Sum') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php $n=0; foreach ($invoice['rows'] as $row):$n++ ?>
        <tr>
            <td><?=$n?>.</td>
            <td><?=$row['invoice_row_description']?></td>
            <td><?=$row['invoice_row_price']?></td>
            <td><?=$row['invoice_row_quantity']?></td>
            <td><?=$row['invoice_row_sum']?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>