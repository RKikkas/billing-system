<?php
/**
 * Created by PhpStorm.
 * User: Roger-PC
 * Date: 12/04/2017
 * Time: 10:16
 */

namespace BillingSystem;


class invoice_Model
{
    static function get($invoice_id)
    {
        $invoices = [];

        // Perform query
        q(  "SELECT *, round((invoice_row_price*invoice_rows.invoice_row_quantity),2) invoice_row_sum
            FROM invoices 
            JOIN customers USING (customer_id)
            JOIN invoice_rows USING (invoice_id)
            WHERE invoice_id = '{$invoice_id}'", $q);

        // Process queryresult into $invoices
        while($row = mysqli_fetch_assoc($q)) {

            $invoices['customer_name'] = $row['customer_name'];

            if (empty ($invoices['sum'])) {
                $invoices['sum'] = $row['invoice_row_sum'];
            }
            else {
                $invoices['sum'] += $row['invoice_row_sum'];
            }

            $invoices['rows'][] = [
                'invoice_row_id' => $row['invoice_row_id'],
                'invoice_row_description' => $row['invoice_row_description'],
                'invoice_row_price' => $row['invoice_row_price'],
                'invoice_row_quantity' => $row['invoice_row_quantity'],
                'invoice_row_sum' => $row['invoice_row_sum'],
            ];
        }
        return $invoices;
    }
}