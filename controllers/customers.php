<?php namespace Halo;

class customers extends Controller
{

    function index()
    {
        $this->customers = get_all("SELECT * FROM customers");
    }

    function view()
    {
        $customer_id = $this->params[0];
        $this->customer = get_first("SELECT * FROM customers WHERE customer_id = '{$customer_id}'");
    }

    function edit()
    {
        $customer_id = $this->params[0];
        $this->customer = get_first("SELECT * FROM customers WHERE customer_id = '{$customer_id}'");
    }

    function post_edit()
    {
        $data = $_POST['data'];
        insert('customer', $data);
    }

    function ajax_delete()
    {
        exit(q("DELETE FROM customers WHERE customer_id = '{$_POST['customer_id']}'") ? 'Ok' : 'Fail');
    }

}