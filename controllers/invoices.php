<?php namespace Halo;

use \BillingSystem\Invoice_Model;

class invoices extends Controller
{

    function index()
    {
        $this->invoices = get_all("
          SELECT *, round(sum(invoice_row_price * invoice_row_quantity),2) sum
          FROM invoices 
          JOIN invoice_rows USING (invoice_id) 
          JOIN customers USING (customer_id)
          GROUP BY invoice_id");
        var_dump($this->invoices);
    }

    function view()
    {
        $invoice_id = $this->params[0];
        $this->invoice = Invoice_Model::get($invoice_id);
    }

    function edit()
    {
        $invoice_id = $this->params[0];
        $this->invoice = get_first("SELECT * FROM invoices WHERE invoice_id = '{$invoice_id}'");
    }

    function post_edit()
    {
        $data = $_POST['data'];
        insert('invoice', $data);
    }

    function ajax_delete()
    {
        exit(q("DELETE FROM invoices WHERE invoice_id = '{$_POST['invoice_id']}'") ? 'Ok' : 'Fail');
    }

    
}